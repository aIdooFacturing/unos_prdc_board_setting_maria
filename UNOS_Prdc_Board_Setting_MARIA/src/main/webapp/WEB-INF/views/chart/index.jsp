<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	var shopId = 1;
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	$(function(){
		setDivPos();
		getCirTime();
		getDvc();
	});
	
	function getCirTime(){
		var url = ctxPath + "/getCirTime.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				$("#time-input").val(data);
			}
		});
	};
	
	function setCirTime(){
		var url = ctxPath + "/setCirTime.do";
		var param = "cirTime=" + $("#time-input").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success"){
					/* alert("저장되었습니다."); */
					// jane 추가
					alert("${saved}");
				}
			}
		});
	};
	
	/* function getIdPush(e, id){
		
		//console.log("e : " + e);
		
		var string = $(e).closest("tr").attr('id');
		
		console.log("string : " + string);
		
		var data = {};
		data.dvcId = id;
		
		if($(e).prop("checked")){
			selectedDvcList.push(data);
			console.log(selectedDvcList);
		}else{
			for(var i=0; i<selectedDvcList.length; i++){
				if(selectedDvcList[i].dvcId == id){
					selectedDvcList.splice(i,1);
				}
			}
			console.log(selectedDvcList);
		}
	}; */
	
	function setSelDvc(){
		
		var dvcId = "";
		var selectedDvcList = [];
		
		for(var i=0; i < dvcLength; i++){
			dvcId = "#dvcId_" + String(i);
			
			var data = {};
			
			if( $(dvcId).children().find("input").prop("checked") ){
				data.dvcId = $(dvcId).attr('class');
				selectedDvcList.push(data);
			}
		}
		
		var selDvc = new Object();
		selDvc.val = selectedDvcList;
		
		var url = ctxPath + "/setSelDvc.do";
		var param = "val=" + JSON.stringify(selDvc); 
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				/* if(data=="success"){
					alert("저장되었습니다.");
				}else if(data=="deleteAll"){
					alert("모두 삭제되었습니다.");
				}else if(data=="nothing"){
					alert("저장된 것이 없습니다.");
				}else{
					alert("에러 발생");
				} */
				
				// jane 추가
				if(data=="success"){
					alert("${saved}");
				}else if(data=="deleteAll"){
					alert("${all_del}");
				}else if(data=="nothing"){
					alert("${no_saved}");
				}else{
					alert("${error}");
				}
			}
		});
	};
	
	var dvcLength = 0;
	
	function getDvc(){
		var url = ctxPath + "/getDvc.do";
		var param = "shopId=" + shopId; 
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"json",
			success : function(data){
				
				var json = data.dataList;
				
				//console.log("length : " + Object.keys(json).length);
				dvcLength = Object.keys(json).length;
				
				console.log("dvcLength : " + dvcLength);
				
				url = ctxPath + "/getSelDvc.do";
				
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType :"json",
					success : function(data2){
						
						var json2 = data2.dataList;
						
						/* var tr = "<thead> <tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" +
											"<td> <input type='checkbox' id='check-all' onclick='checkAll()'> </td>" +
											"<td>장비명</td>" +
										 "</tr></thead><tbody>"; */
										 
						var tr = "<thead> <tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" +
											"<td> <input type='checkbox' id='check-all' onclick='checkAll()'> </td>" +
											"<td>" + "${device}" + "</td>" +
										 "</tr></thead><tbody>";
					 
						$(json).each(function(idx, data){
							
							var isAdd = 0;
							
							$(json2).each(function(idx2, data2){
								
								if(data.dvcId == data2.dvcId){
									
									isAdd = 1;
// 									console.log(data.dvcId + " = " + data2.dvcId);
								}
							});
							
							if(isAdd == 1){
								tr += "<tr id='dvcId_"+ idx +"' class='"+ data.dvcId +"'><td><input type='checkbox' checked ></td>" +
	  									    "<td>" + data.name + "</td>" + 
									  "</tr>";
							  
							}else{
								tr += "<tr id='dvcId_"+ idx +"' class='"+ data.dvcId +"'><td><input type='checkbox' ></td>" +
											"<td>" + data.name + "</td>" + 
									  "</tr>";
							}
						});
						
						tr += "</tobdy>";
						 
						$("#dvcTable").html(tr).css({
							"font-size": getElSize(40)
						});
						
						$("#dvcTable td").css({
							"padding" : getElSize(20),
							"font-size": getElSize(40),
							"border": getElSize(5) + "px solid rgb(50,50,50)"
						});
						
						
						
					}
				});
			}
		});
	};
	
	function checkAll(){
		
		//console.log($("#check-all").is(":checked"));
		
		if($("#check-all").is(":checked") == true){
			$("input:checkbox").prop("checked", true);
		}else{
			$("input:checkbox").prop("checked", false);
		}
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	
	function setDivPos(){
		$("#title_div").css({
			"font-size" : getElSize(80),
			"margin-top" : getElSize(52)
		});
		
		$("#input-div").css({
			"height" : getElSize(210),
			"font-size" : getElSize(80)
		});
		
		$("#wraper").css({			
			"color" : "white",
			"margin-left" : getElSize(70),
			"margin-top" : getElSize(23)
		});
		
		$("#time-input").css({
			"border" : getElSize(0),						
			"font-size" : getElSize(100),
			"width" : getElSize(181),
			"height" : getElSize(100),
			"text-align" : "center"
		});
		
		$("#cirTime_save").css({
			"font-size" : getElSize(70),
			"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
			"color" : "rgb(34,34,34)",
			"border" : "1px solid white",
			"margin-left" : getElSize(50),
			"border-radius" : getElSize(10),
			//"margin-top" : getElSize(50),
			"width" : getElSize(300)
		}).hover(function(){
			$(this).css({
				"color" : "white",
			})
		}, function(){
			$(this).css({
				"color" : "rgb(34,34,34)",
			})
		});
		
		$("#selDvc_save").css({
			"font-size" : getElSize(70),
			"background" : "linear-gradient(#bfbfbf, #9e9e9e)",
			"color" : "rgb(34,34,34)",
			"border" : "1px solid white",
			"margin-left" : getElSize(550),
			"border-radius" : getElSize(10),
			//"margin-top" : getElSize(50),
			"width" : getElSize(300)
		}).hover(function(){
			$(this).css({
				"color" : "white",
			})
		}, function(){
			$(this).css({
				"color" : "rgb(34,34,34)",
			})
		});
		
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		};
		
		// jane 추가
		$("button").css({
			"padding" : getElSize(15) + "px",
			"font-size" : getElSize(50) + "px",
			"background" : "linear-gradient(rgb(191, 191, 191), rgb(158, 158, 158))", 
			"color" : "rgb(34, 34, 34)",
			"border" : "1px solid white",
			"border-radius" : getElSize(10) + "px",
			"width" : getElSize(200) + "px"
		});
		
		if(getParameterByName('lang')=='de') {
			$("button").css({
				"width" : "auto"
			});
		}
		
		if(getParameterByName('lang')=='de') {
			$("#selDvc_save").css({
				"margin-left" : getElSize(380) + "px"
			});
		} else if(getParameterByName('lang')=='en') {
			$("#selDvc_save").css({
				"margin-left" : getElSize(290) + "px"
			});
		}
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/index.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow-x : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
input[type="checkbox"] {
	transform: scale(1.2)
}

</style> 

</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	
	<div id="time"></div>
	<div id="title"><spring:message code="prdc_board_setting"></spring:message></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<div style="width: 90%" id="wraper"> 
						<div id="input-div">
							<!-- <label>장비 자동 순환 시간 : <input class="input" id="time-input" type="text"> 초 </label> -->
							<!-- jane 추가 --> 
							<label> <spring:message code="machine_auto_circulation_time"></spring:message> : <input class="input" id="time-input" type="text"> <spring:message code="second"></spring:message> </label>
							<button onclick="setCirTime();" id="cirTime_save"><spring:message code="save"></spring:message> </button>
						</div>
						<hr>
						<div id="title_div">
							<!-- <label>출력 장비 선택</label> -->
							<!-- jane 추가 -->
							<label> <spring:message code="output_machine_select"></spring:message> </label>
							<button onclick="setSelDvc();" id="selDvc_save"><spring:message code="save"></spring:message> </button>
						</div>
						<div id="dvc_div">
							<table style="width: 50%; color: white; text-align: center; border-collapse: collapse;" id="dvcTable" border="1">
							</table>
						</div>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span" ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	