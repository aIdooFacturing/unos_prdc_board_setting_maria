var appServerUrl = null;

$(function() {
	createNav("config_nav", 3);
	setEl();
	time();

	window.setInterval(function() {
		var width = window.innerWidth;
		var height = window.innerHeight;

		if (width != originWidth || height != originHeight) {
			location.reload();
		}
		;
	}, 1000 * 10);
});


var handle = 0;
function time(){
	$("#time").html(getToday());
	 handle = requestAnimationFrame(time)
};

function setEl(){
	var neonColor = "#0096FF";
	
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	$(".right").css({
		"height" : getElSize(120)
	});
	
	$(".left, .menu_left").css({
		"width" : getElSize(495)			
	})
	
	$("#container").css({
		"width" : contentWidth,
		"height" : contentHeight,
	});
	
	$("#container").css({
		"margin-left" : (originWidth/2) - ($("#container").width()/2),
		"margin-top" : (originHeight/2) - ($("#container").height()/2)
	})
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.5,
		"position" : "absolute",
		//"background-color" : "black",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	})
	
	$("#time").css({
		"color" : "white",
		"position" : "absolute",
		"font-size" : getElSize(40),
		"top" : getElSize(25) + marginHeight,
		"right" : getElSize(30) + marginWidth
	});
	
	$("#table").css({
		"position" : "absolute",
		"width" : $("#container").width(),
		"top" : getElSize(100) + marginHeight
	});
	
	$("#table2 td").css({
		"padding" : getElSize(20),
		"font-size": getElSize(40),
		"border": getElSize(5) + "px solid black"
	});
	
	
	$(".right").css({
		"width" : contentWidth - $(".left").width() 
	});
	
	$(".menu_right").css({
		"width" : $(".right").width()
	})
	
	$("#home").css({
		"cursor" : "pointer"
	})
	
	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"color" : "white",
		"font-size" : getElSize(40),
		"top" : getElSize(130) + marginHeight,
		"right" : getElSize(30) + marginWidth
	});
	
	$("span").css({
		"color" : "#8D8D8D",
		"position" : "absolute",
		"font-size" : getElSize(45),
		"margin-top" : getElSize(20),
		"margin-left" : getElSize(20)
	});
	
	$("#selected").css({
		"color" : "white",
	});
	
	$("span").parent("td").css({
		"cursor" : "pointer"
	});
	
	$(".title_span").css({
		"color" : "white",
		"font-size" : getElSize(40),
		"background-color" : "#353535",
		"padding" : getElSize(15)
	});
		
	$("#search").css({
		"cursor" : "pointer",
		"width" : getElSize(80),
	});
	
	$("#content_table td, #content_table2 td").css({
		"color" : "#BFBFBF",
		"font-size" : getElSize(50)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		//"z-index" : -1,
		"display" : "none",
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	$("#contentDiv").css({
		"overflow" : "auto",
		"width" : $(".menu_right").width()
	});
	
	$("#insertForm").css({
		"width" : getElSize(3000),
		"position" : "absolute",
		"z-index" : 999,
	});
	
	$("#insertForm").css({
		"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
		"top" : getElSize(400)
	});
	
	$("#insertForm table td").css({
		"font-size" : getElSize(70),
		"padding" : getElSize(15),
		"background-color" : "#323232"
	});
	
	$("#insertForm button, #insertForm select, #insertForm input").css({
		"font-size" : getElSize(60),
		"margin" : getElSize(15)
	});
	
	$(".table_title").css({
		"background-color" : "#222222",
		"color" : "white"
	});
	
	$("#contentTable td").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(100),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#app_store_iframe").css({
		"width" : getElSize(3100) + "px",
		"height" : getElSize(2000) + "px",
		"position" : "absolute",
		"z-index" : 9999,
		"display" : "block"
	});
	
	$("#app_store_iframe").css({
		"left" : originWidth * 1.5,
		"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
	}).attr("src", appServerUrl)
	
	
	$("#delDiv").css({
		"color" : "white",
		"z-index" : 9999999,
		"background-color" : "black",
		"position" : "absolute",
		"width" : getElSize(700) + "px",
		"font-size" : getElSize(60) + "px",
		"text-align" : "center",
		"padding" : getElSize(30) + "px",
		"border" : getElSize(7) + "px solid rgb(34,34,34)",
		"border-radius" : getElSize(50) + "px"
	});
	
	
	$("#delDiv div").css({
		"background-color" : "rgb(34,34,34)",
		"padding" : getElSize(20) + "px",
		"margin" : getElSize(10) + "px",
		"cursor" : "pointer"
	}).hover(function(){
		$(this).css({
			"background-color" : "white",
			"color" : "rgb(34,34,34)",
		})
	}, function(){
		$(this).css({
			"background-color" : "rgb(34,34,34)",
			"color" : "white",
		})
	});
	
	$("#delDiv").css({
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"top" : - $("#delDiv").height() * 2
	});
	
	$("#delDiv div:nth(1)").click(function(){
		$("#delDiv").animate({
			"top" : - $("#delDiv").height() * 2
		});
	});
	
}