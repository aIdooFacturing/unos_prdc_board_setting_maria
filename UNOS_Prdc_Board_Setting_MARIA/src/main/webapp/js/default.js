var appVer = "v.1.0 ";
var shopId = 1;

var is_login = window.sessionStorage.getItem("login");
function createCorver(){
	var corver = document.createElement("div");
	corver.setAttribute("id", "corver");
	corver.style.cssText = "width : " + originWidth + 
						"; height : " + originHeight + 
						"; position : absolute" + 
						"; z-index : 99999999" + 
						"; background-color : black";
	
	$("body").prepend(corver);
};

function login(){
	var url = ctxPath + "/login.do";
	var $id = $("#email").val();
	var $pwd = $("#pwd").val();
	var param = "id=" + $id + 
				"&pwd=" + $pwd + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				window.sessionStorage.setItem("login_time", new Date().getTime());
				window.sessionStorage.setItem("login", "success")
				window.sessionStorage.setItem("user_id", $id)
				loginSuccess();
				drawLogout();
			}else{
				$("#errMsg").html("계정 정보가 올바르지 않습니다.")
			}
		}
	});
	
//	if($id == id && $pwd == pwd){
//		
//	}else{
//		
//	}
//	var url = "${ctxPath}/chart/chkLogin.do";
//	var param = "id=" + $("#email").val() + 
//				"&pwd=" + $("#pwd").val();
//	
//	$.ajax({
//		url : url,
//		data : param,
//		type : "post",
//		dataType : "text",
//		success : function(data){
//			if(data!="fail" && data!=""){
//				loginSuccess();
//				window.sessionStorage.setItem("login", "success");
//				window.sessionStorage.setItem("lv", data);
//				window.sessionStorage.setItem("id", $("#email").val());
//				window.sessionStorage.setItem("login_time", new Date().getTime());
//				login_lv = data;
//			}else{
//				//alert("계정 정보가 올바르지 않습니다.")
//				$("#errMsg").html("계정 정보가 올바르지 않습니다.")
//			}
//		}
//	});
};

function loginSuccess(){ 
//	$("img, span").css({
//		"transition" : "0.5s",
//		"-webkit-filter" : "blur(0px)"
//	});
	
	$("#loginForm").css({
		"z-index" : "-99",
		"opacity" : 0
	});
	
	closeCorver();
};

function chkKeyCd(evt){
	if(evt.keyCode==13) login();
};

function createLoginForm(){
	var loginForm = "<div id='loginForm' style='display: none'>" +
						"<form id='login' onsubmit='return false;'>" +
							"<Center>" +
								"<img src='" + ctxPath + "/images/SmartFactory.png' id='logo'><br>" +
								"<img src='" + ctxPath + "/images/logo.png' id='logo2'><br>" +
								"<input type='email' placeholder='ID' id='email' data-role='none'  onkeyup='chkKeyCd(event)'>" +
								"<input type='password' placeholder='Password' id='pwd' data-role='none' onkeyup='chkKeyCd(event)'><br>" +
								"<br>" +
								"<img src='" + ctxPath + "/images/enter.png' id='enter' onclick='login()' style='cursor : pointer;'>" +
								"<br>" +
								"<hr id='hr'>" +
								"<div id='errMsg' class='errMsg'></div>" +
							"</Center>" +
						"</form>" +
					"</div>";
							
							
	$("body").prepend(loginForm);
	
	$("#enter").css({
		"width" : getElSize(200)
	});
	
	$("#loginForm").css({
		"width" : getElSize(1000),
		"height" : getElSize(900),
		"z-index" : 99999,
		"background-color" : "white", 
		"position" : "absolute",
		"border-radius" : getElSize(30) + "px"
	});
	
	$("#loginForm").css({
		"left" : (window.innerWidth/2) - ($("#loginForm").width()/2),
		"top" : (window.innerHeight/2) - ($("#loginForm").height()/2)
	});
	
	$("#login #email").css({
		"height" :  getElSize(150) + "px",
		"width" : getElSize(700) + "px", 
		"font-size" : getElSize(50) + "px", 
		"padding" :  getElSize(30) + "px" , 
		"border-top-left-radius" : getElSize(30) + "px", 
		"border-top-right-radius" : getElSize(30) + "px", 
		"outline" : "0", 
		"border" :  getElSize(3) + "px solid rgb(235,234,219)"
	}).focus();

	$("#login #pwd").css({
		"height" :  getElSize(150) + "px",
		"width" : getElSize(700) + "px", 
		"font-size" : getElSize(50) + "px", 
		"padding" :  getElSize(30) + "px" , 
		"border-bottom-left-radius" : getElSize(30) + "px", 
		"border-bottom-right-radius" : getElSize(30) + "px", 
		"outline" : "0", 
		"border" :  getElSize(5) + "px solid rgb(235,234,219)"
	}); 

	$("#logo").css({ 
		"width" : getElSize(500) + "px",
		"margin-top" : getElSize(50) + "px"
	});

	$("#logo2").css({
		"width" : getElSize(300) + "px", 
		"margin-bottom" : getElSize(50) + "px",
		"-ms-interpolation-mode" : "bicubic"
	});

	$("#hr").css({
		"margin-top" : getElSize(50) + "px",
		"width" : "80%"	,
		"border" : "1px solid black"
	});

	$("#saveIdText").css({
		"margin-top" : getElSize(20),
		"font-size" : getElSize(40)	
	});
	
	$(".errMsg").css({
		"color" : "red", 
		"font-size" : getElSize(40) + "px"	
	});
};

$(function(){
	$("#home").click(function(){
		var url = location.href.substr(0,location.href.indexOf(ctxPath));
		location.href = "/iDOO_CONTROL/chart/index.do";
	});
	
	//$("#title_right").html(comName);
	createCorver();
	createLoginForm();
	getComName();
	bindEvt();
	drawFlag();
	
	
	var is_login = window.sessionStorage.getItem("login");
	var login_time = window.sessionStorage.getItem("login_time");
	var user_id = window.sessionStorage.getItem("user_id");
	var time = new Date().getTime();
	//if(login==null || (login !=null && (time - login_time) / 1000 > 60 * 60 * 24)) {
	if(is_login==null) {
		location.href = "/iDOO_CONTROL/chart/index.do";
//		showCorver();
//		$("#loginForm").css("display", "block");
//		$("#email").focus();
	}else{
		if(user_id != 'admin'){
			alert("관리자 아이디로 로그인하세요!");
			location.href = "/iDOO_CONTROL/chart/index.do";
		}else{
			loginSuccess();
			drawLogout();
		}
	}
	//loginSuccess();
	chkBanner();
});

function drawLogout(){
	var logout = "<img src=" + ctxPath + "/images/logout.png id='logout' />";
	$("body").prepend(logout);
	$("#logout").css({
		"position" : "absolute",
		"left" : $("#flagDiv").offset().left + $("#flagDiv").width() + getElSize(50),
		"top" : marginHeight + getElSize(20),
		"width" : getElSize(200),
		"cursor" : "pointer"
	}).click(function(){
		window.sessionStorage.removeItem("login");
		window.sessionStorage.removeItem("login_time");
		window.sessionStorage.removeItem("user_id");
		//location.reload()
		location.href = "/iDOO_CONTROL/chart/index.do";
	})
	
	var userId = window.sessionStorage.getItem("user_id")
	var id = "<font id='id_font'>ID : " + userId + "</font><font id='appVer'> " + appVer + "</font>";
	$("body").append(id);
	
	$("#id_font").css({
		"position" : "absolute",
		"left" : $("#flagDiv").offset().left + $("#flagDiv").width() + getElSize(270),
		"top" : marginHeight + getElSize(20),
		"color" : "white",
		"font-size" : getElSize(50),
	});
	
	$("#appVer").css({
		"position" : "absolute",
		"left" : $("#id_font").offset().left + $("#id_font").width(),
		"top" : marginHeight + getElSize(20),
		"color" : "black",
		"font-size" : getElSize(50),
	});
};

function drawFlag(){
	var ko = "<img src=" + ctxPath + "/images/ko.png id='ko' class='flag'/>";
	var cn = "<img src=" + ctxPath + "/images/cn.png id='cn' class='flag'/>";
	var en = "<img src=" + ctxPath + "/images/en.png id='en' class='flag'/>";
	var de = "<img src=" + ctxPath + "/images/de.png id='de' class='flag'/>";
	
	var div = "<div id='flagDiv'>" + ko 
									+ cn 
									+ en 
									+ de
									+ "</div>";
	$("body").prepend(div);
	
	$("#flagDiv").css({
		"position" : "absolute",
		"left" : marginWidth,
		"top" : marginHeight + getElSize(20)
	});
	
	$(".flag").css({
		"width" : getElSize(100),
		"cursor" : "pointer"
	}).click(changeLang);
	
	$(".flag").css("filter", "grayscale(100%)")
	var lang = window.localStorage.getItem("lang");
	$("#" +lang).css("filter", "grayscale(0%)");
};

function bindEvt(){
};

function changeLang(){
	var lang = this.id;
	var url = window.location.href;
	var param = "?lang=" + lang;
	
	window.localStorage.setItem("lang", lang)
	
	if(url.indexOf("fromDashBoard")!=-1){
		param = "&lang=" + lang;
		if(url.indexOf("&lang")!=-1){
			url = url.substr(0, url.lastIndexOf("&lang"));
		}
	}else{
		url = url.substr(0, url.lastIndexOf("?"))
	}
	
	location.href = url + param;
	
	$(".flag").css("filter", "grayscale(100%)")
	$("#" +lang).css("filter", "grayscale(0%)");
}

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};

function getComName(){
	var url = ctxPath + "/getComName.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			$("#title_right").html(decode(data));
		}
	});
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
}

function showCorver(){
	$("#corver").css({
		"z-index" : 9999,
		"background-color" : "black",
		//"opacity" : 0.6
	});
};

function closeCorver(){
	$("#corver").css({
		"z-index" : -999,
	});
}


function getBanner(){
	var url = ctxPath + "/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100); 
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			//twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = false;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 300)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},8000 * 2, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};