package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	
	private final static String namespace= "com.unomic.dulink.chart.";
	private static final Logger logger = LoggerFactory.getLogger(ChartServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSessionTemplate_app_server")
	private SqlSession app_server_sql;
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	
	@Override
	public String getAppList(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList  = app_server_sql.selectList(namespace + "getAppList", chartVo);
	    
		List list = new ArrayList<ChartVo>();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			
			map.put("id", dataList.get(i).getId());
			map.put("appId", dataList.get(i).getAppId());
			map.put("name", dataList.get(i).getAppName());
			map.put("url", dataList.get(i).getUrl());
			
			list.add(map);
		}; 
  
		Map dataMap = new HashMap(); 
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String removeApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "removeApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}       

	@Override
	public String addNewApp(ChartVo chartVo) throws Exception {
		String str = "";
		try{
			app_server_sql.delete(namespace + "addNewApp", chartVo);
			str = "success";
		}catch(Exception e){
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@Override     
	public String getTgCnt(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> dataList  = sql.selectList(namespace + "getTgCnt", chartVo);
	    
		List list = new ArrayList<ChartVo>();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", dataList.get(i).getName());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
			map.put("tgRunTime", dataList.get(i).getTgRunTime());
			//map.put("workIdx", dataList.get(i).getWorkIdx());
			
			
			list.add(map);
		}; 
  
		Map dataMap = new HashMap(); 
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
	
		return str;
	}

	@Override
	public String addTgCnt(String val, ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();

		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo2 = new ChartVo();
			chartVo2.setDvcId(tempObj.get("dvcId").toString());
			chartVo2.setWorkDate(chartVo.getWorkDate());
			//chartVo.setWorkIdx(tempObj.get("workIdx").toString());
			chartVo2.setTgCnt(tempObj.get("tgCnt").toString());
			chartVo2.setCntPerCyl(tempObj.get("cntPerCyl").toString());
			chartVo2.setTgRunTime(tempObj.get("tgRunTime").toString());
			
			list.add(chartVo2);
			//logger.info("chartVo: " + chartVo2);
		}
		
		//logger.info("list: " + list);

		String str = "";

		try {
			sql.delete(namespace + "delTgCnt", chartVo);
			sql.insert(namespace + "addTgCnt", list);
			str = "success";
		}catch (Exception e) {
			e.printStackTrace();
			str = "fail";
		}

		return str;
	}
	
	@Override
	public String getCirTime() throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getCirTime");
		
		return rtn;
	}
	
	@Override
	public String setCirTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = "";
		
		try {
			sql.insert(namespace + "setCirTime", chartVo);
			rtn = "success";
		}catch(Exception e) {
			rtn = "fail";
		};
		
		return rtn;
	}
	
	@Override
	public String getDvc(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> dataList = sql.selectList(namespace + "getDvc", chartVo);
		List list = new ArrayList<ChartVo>();
		
		for(int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", dataList.get(i).getName());
			
			list.add(map);
		};
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}
	
	@Override
	public String setSelDvc(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		
		//System.out.println(val);
		//logger.info(val);
		
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		
		List<ChartVo> list = new ArrayList<ChartVo>();
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setDvcId(tempObj.get("dvcId").toString());

			list.add(chartVo);
		}
		
		//logger.info("list: " + list);
		
		int cntDel = 0;
		int cntInst = 0;
		String result = "";
		
		cntDel = sql.delete(namespace + "delSelDvc");
		
		if(list.size() > 0) {
			cntInst = sql.insert(namespace+"setSelDvc", list);
		}
		
		//logger.info("list size : " + list.size());
		//logger.info("cntDel : " + cntDel + ", cntInst : " + cntInst);
		
		
		if(cntDel == 0 && cntInst == 0) {
			result = "nothing";	// 아무값도 삭제하거나 추가한 것이 없음 
		}else if(cntDel != 0 && cntInst == 0) {
			result = "deleteAll";	// 모두 삭제하였으나 추가한 것은 없음 
		}else if(cntInst != 0) {
			result = "success";		// 추가함 
		}
		
		//logger.info("result : " + result);
		
		return result;
	}
	
	@Override
	public String getSelDvc() throws Exception {
		SqlSession sql = getSqlSession();
		
		List <ChartVo> dataList = sql.selectList(namespace + "getSelDvc");
		List list = new ArrayList<ChartVo>();
		
		for(int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("dvcId", dataList.get(i).getDvcId());
			
			list.add(map);
		};
		
		//logger.info("list: " + list);
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		
		return str;
	}
	
	//common func
	
	


};